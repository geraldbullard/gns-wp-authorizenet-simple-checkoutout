<?php
if (isset($_GET['settings-updated']) && $_GET['settings-updated'] == 'true'):
    echo '<div id="message" class="updated notice notice-success is-dismissible">
        <p>Settings saved.</p>
        <button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button>
    </div>';
endif;
?>
<form method="post" action="options.php">
    <?php settings_fields('gns-settings-group'); ?>
    <?php do_settings_sections('gns-settings-group'); ?>
    <table class="form-table">
        <tr valign="top">
            <th scope="row">Processor Description</th>
            <td>
                <input
                    type="text"
                    name="gns_checkout_processor_description"
                    value="<?php echo get_option('gns_checkout_processor_description'); ?>" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Login ID</th>
            <td>
                <input
                    type="text"
                    name="gns_checkout_auth_login_id"
                    value="<?php echo get_option('gns_checkout_auth_login_id'); ?>"
                    placeholder="API Login ID" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Transaction Key</th>
            <td>
                <input
                    type="text"
                    name="gns_checkout_auth_transaction_key"
                    value="<?php echo get_option('gns_checkout_auth_transaction_key'); ?>"
                    placeholder="API Transaction Key" />
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Mode(Live/Test Sandbox)</th>
            <td>
                <select name="gns_checkout_auth_mode" />
                <option
                    value="live"
                    <?php if (get_option('gns_checkout_auth_mode') == "live"): echo 'selected'; endif; ?>>
                    Live
                </option>
                <option
                    value="test"
                    <?php if (get_option('gns_checkout_auth_mode') == "test"): echo 'selected'; endif; ?>>
                    Test/Sandbox
                </option>
                </select>
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Include Shipping Fields</th>
            <td>
                <input
                    type="radio"
                    name="gns_checkout_include_shipping"
                    value="yes"
                    <?php
                    echo (get_option('gns_checkout_include_shipping') == 'yes' ? 'checked="checked"' : '');
                    ?> />Yes
                <input
                    type="radio"
                    name="gns_checkout_include_shipping"
                    value="no"
                    <?php
                    echo (get_option('gns_checkout_include_shipping') == 'no' ? 'checked="checked"' : '');
                    ?> />No
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Include Billing Company</th>
            <td>
                <input
                    type="radio"
                    name="gns_checkout_include_billing_company"
                    value="yes"
                    <?php
                    echo (get_option('gns_checkout_include_billing_company') == 'yes' ? 'checked="checked"' : '');
                    ?> />Yes
                <input
                    type="radio"
                    name="gns_checkout_include_billing_company"
                    value="no"
                    <?php
                    echo (get_option('gns_checkout_include_billing_company') == 'no' ? 'checked="checked"' : '');
                    ?> />No
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Include Shipping Company</th>
            <td>
                <input
                    type="radio"
                    name="gns_checkout_include_shipping_company"
                    value="yes"
                    <?php
                    echo (get_option('gns_checkout_include_shipping_company') == 'yes' ? 'checked="checked"' : '');
                    ?> />Yes
                <input
                    type="radio"
                    name="gns_checkout_include_shipping_company"
                    value="no"
                    <?php
                    echo (get_option('gns_checkout_include_shipping_company') == 'no' ? 'checked="checked"' : '');
                    ?> />No
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">
                Include Optional Information<br />
                <small>(Phone | Email | PO/Invoice Number)</small>
            </th>
            <td>
                <input
                    type="radio"
                    name="gns_checkout_include_optional"
                    value="yes"
                    <?php
                    echo (get_option('gns_checkout_include_optional') == 'yes' ? 'checked="checked"' : '');
                    ?> />Yes
                <input
                    type="radio"
                    name="gns_checkout_include_optional"
                    value="no"
                    <?php
                    echo (get_option('gns_checkout_include_optional') == 'no' ? 'checked="checked"' : '');
                    ?> />No
            </td>
        </tr>
        <tr valign="top">
            <th scope="row">Thank You Message</th>
            <td>
                <?php
                wp_editor(get_option('gns_checkout_thank_you_message'), 'gns_checkout_thank_you_message');
                ?>
            </td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>