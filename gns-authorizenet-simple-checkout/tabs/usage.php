<div class="gns-admin-settings-padding">
    <p class="gns-plugin-welcome">
        Thank you for installing the <a><strong>Authorize.Net Simple Checkout</strong></a> plugin by gnsPLANET. We hope
        you find this plugin easy to configure so you can get started using it right away!<br />
        Let's get started...
    </p>
    <p>
        <ol class="">
            <li>
                During the plugin installation a new page was created called "<a href="<?php
                echo get_option('site_url') . '/simple-checkout/';
                ?>" target="_blank"><strong>Authorize.Net Simple Checkout</strong></a>".
            </li>
            <li>
                This page will be your Shopping Cart/Checkout page on the front end of your site.
            </li>
            <li>
                This page is setup to accept specific $_POST values to populate the form before submission to the
                Authorize.Net Payment API.
            </li>
            <li>
                The following code snippet is the simplest form you can use to send the cart total
                ( gns_checkout_cart_total ) to the checkout page.
            </li>
        </ol>
    </p>
    <p class="gns-admin-code-fragment">
        <?php echo htmlentities('<form action="/simple-checkout/" method="post">'); ?><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<?php echo htmlentities('<input type="hidden" value="3.67" name="gns_checkout_cart_total" />'); ?><br />
        &nbsp;&nbsp;&nbsp;&nbsp;<?php echo htmlentities('<button type="submit" value="Checkout">Checkout</button>'); ?><br />
        <?php echo htmlentities('</form>'); ?>
    </p>
</div>