<?php
/**
Plugin Name: Authorize.net Simple Checkout
Author: gnsPLANET
Author URI: https://www.weblandhosting.com/
Plugin URL: https://www.weblandhosting.com/
Description: Accept payments simply with Authorize.net.
Version: 9.9.9
License: GPLv2 or later
 */

/**
 * Start the session for the cart total and items.
 */
function start_session() {
    if(!session_id()) {
        session_start();
    }
}
add_action('init', 'start_session', 1);

/**
 * Register the orders post type.
 */
function gns_checkout_type () {
    $labels = array(
        'name' => _x('Orders', 'Post Type General Name', 'gns_checkout'),
        'singular_name' => _x('Order', 'Post Type Singular Name', 'gns_checkout'),
        'menu_name' => __('Orders', 'gns_checkout'),
        'parent_item_colon' => __('Parent Order', 'gns_checkout'),
        'all_items' => __('All Orders', 'gns_checkout'),
        'view_item' => __('View Order', 'gns_checkout'),
        'add_new_item' => __('Add New Order', 'gns_checkout'),
        'add_new' => __('Add New', 'gns_checkout'),
        'edit_item' => __('Edit Order', 'gns_checkout'),
        'update_item' => __('Update Order', 'gns_checkout'),
        'search_items' => __('Search Order', 'gns_checkout'),
        'not_found' => __('Not found', 'gns_checkout'),
        'not_found_in_trash' => __('Not found in Trash', 'gns_checkout'),
    );
    $args = array(
        'label' => __('gns_checkout', 'gns_checkout'),
        'description' => __('list of orders', 'gns_checkout'),
        'menu_icon' => 'dashicons-cart',
        'labels' => $labels,
        'supports' => array('title', 'custom-fields'),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 45,
        'can_export' => true,
        'has_archive' => false,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'capabilities' => array( // hide add new
            'create_posts' => false
        ),
        'map_meta_cap' => true, // supports no new posts for capabilities
    );
    register_post_type('gns_checkout', $args);
}
add_action('init', 'gns_checkout_type', 0);

/**
 * States Array [Code/Title] (US Only)
 */
$us_states = array(
    'AL' => 'Alabama', 'AK' => 'Alaska', 'AZ' => 'Arizona', 'AR' => 'Arkansas', 'CA' => 'California',
    'CO' => 'Colorado', 'CT' => 'Connecticut', 'DE' => 'Delaware', 'DC' => 'District of Columbia',
    'FL' => 'Florida', 'GA' => 'Georgia', 'HI' => 'Hawaii', 'ID' => 'Idaho', 'IL' => 'Illinois',
    'IN' => 'Indiana', 'IA' => 'Iowa', 'KS' => 'Kansas', 'KY' => 'Kentucky', 'LA' => 'Louisiana',
    'ME' => 'Maine', 'MD' => 'Maryland', 'MA' => 'Massachusetts', 'MI' => 'Michigan', 'MN' => 'Minnesota',
    'MS' => 'Mississippi', 'MO' => 'Missouri', 'MT' => 'Montana', 'NE' => 'Nebraska', 'NV' => 'Nevada',
    'NH' => 'New Hampshire', 'NJ' => 'New Jersey', 'NM' => 'New Mexico', 'NY' => 'New York', 'NC' => 'North Carolina',
    'ND' => 'North Dakota', 'OH' => 'Ohio', 'OK' => 'Oklahoma', 'OR' => 'Oregon', 'PA' => 'Pennsylvania',
    'RI' => 'Rhode Island', 'SC' => 'South Carolina', 'SD' => 'South Dakota', 'TN' => 'Tennessee', 'TX' => 'Texas',
    'UT' => 'Utah', 'VT' => 'Vermont', 'VA' => 'Virginia', 'WA' => 'Washington', 'WV' => 'West Virginia',
    'WI' => 'Wisconsin', 'WY' => 'Wyoming',
);

/**
 * Get State Name from Code
 */
function get_state_name($code='') {
    global $us_states;
    foreach ($us_states as $k => $v) {
        if ($code == $k) {
            $sname = $v;
        }
    }
    return $sname;
}

/**
 * State Code Array (US Only)
 */
$us_state_codes = array(
    'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA',
    'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR',
    'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'
);

/**
 * Countries Array [Code/Title]
 */
$countries = array(
    "AF" => "Afghanistan", "AL" => "Albania", "DZ" => "Algeria", "AS" => "American Samoa", "AD" => "Andorra",
    "AO" => "Angola", "AI" => "Anguilla", "AQ" => "Antarctica", "AG" => "Antigua and Barbuda", "AR" => "Argentina",
    "AM" => "Armenia", "AW" => "Aruba", "AU" => "Australia", "AT" => "Austria", "AZ" => "Azerbaijan",
    "BS" => "Bahamas", "BH" => "Bahrain", "BD" => "Bangladesh", "BB" => "Barbados", "BY" => "Belarus",
    "BE" => "Belgium", "BZ" => "Belize", "BJ" => "Benin", "BM" => "Bermuda", "BT" => "Bhutan",
    "BO" => "Bolivia", "BA" => "Bosnia and Herzegovina", "BW" => "Botswana", "BV" => "Bouvet Island", "BR" => "Brazil",
    "BQ" => "British Antarctic Territory", "IO" => "British Indian Ocean Territory", "VG" => "British Virgin Islands",
    "BN" => "Brunei", "BG" => "Bulgaria", "BF" => "Burkina Faso", "BI" => "Burundi", "KH" => "Cambodia",
    "CM" => "Cameroon", "CA" => "Canada", "CT" => "Canton and Enderbury Islands", "CV" => "Cape Verde",
    "KY" => "Cayman Islands", "CF" => "Central African Republic", "TD" => "Chad", "CL" => "Chile", "CN" => "China",
    "CX" => "Christmas Island", "CC" => "Cocos [Keeling] Islands", "CO" => "Colombia", "KM" => "Comoros",
    "CG" => "Congo - Brazzaville", "CD" => "Congo - Kinshasa", "CK" => "Cook Islands", "CR" => "Costa Rica",
    "HR" => "Croatia", "CU" => "Cuba", "CY" => "Cyprus", "CZ" => "Czech Republic", "CI" => "Cote d'Ivoire",
    "DK" => "Denmark", "DJ" => "Djibouti", "DM" => "Dominica", "DO" => "Dominican Republic",
    "NQ" => "Dronning Maud Land", "DD" => "East Germany", "EC" => "Ecuador", "EG" => "Egypt", "SV" => "El Salvador",
    "GQ" => "Equatorial Guinea", "ER" => "Eritrea", "EE" => "Estonia", "ET" => "Ethiopia", "FK" => "Falkland Islands",
    "FO" => "Faroe Islands", "FJ" => "Fiji", "FI" => "Finland", "FR" => "France", "GF" => "French Guiana",
    "PF" => "French Polynesia", "TF" => "French Southern Territories", "FQ" => "French Southern and Antarctic Territories",
    "GA" => "Gabon", "GM" => "Gambia", "GE" => "Georgia", "DE" => "Germany", "GH" => "Ghana", "GI" => "Gibraltar",
    "GR" => "Greece", "GL" => "Greenland", "GD" => "Grenada", "GP" => "Guadeloupe", "GU" => "Guam", "GT" => "Guatemala",
    "GG" => "Guernsey", "GN" => "Guinea", "GW" => "Guinea-Bissau", "GY" => "Guyana", "HT" => "Haiti",
    "HM" => "Heard Island and McDonald Islands", "HN" => "Honduras", "HK" => "Hong Kong SAR China", "HU" => "Hungary",
    "IS" => "Iceland", "IN" => "India", "ID" => "Indonesia", "IR" => "Iran", "IQ" => "Iraq", "IE" => "Ireland",
    "IM" => "Isle of Man", "IL" => "Israel", "IT" => "Italy", "JM" => "Jamaica", "JP" => "Japan", "JE" => "Jersey",
    "JT" => "Johnston Island", "JO" => "Jordan", "KZ" => "Kazakhstan", "KE" => "Kenya", "KI" => "Kiribati",
    "KW" => "Kuwait", "KG" => "Kyrgyzstan", "LA" => "Laos", "LV" => "Latvia", "LB" => "Lebanon", "LS" => "Lesotho",
    "LR" => "Liberia", "LY" => "Libya", "LI" => "Liechtenstein", "LT" => "Lithuania", "LU" => "Luxembourg",
    "MO" => "Macau SAR China", "MK" => "Macedonia", "MG" => "Madagascar", "MW" => "Malawi", "MY" => "Malaysia",
    "MV" => "Maldives", "ML" => "Mali", "MT" => "Malta", "MH" => "Marshall Islands", "MQ" => "Martinique",
    "MR" => "Mauritania", "MU" => "Mauritius", "YT" => "Mayotte", "FX" => "Metropolitan France", "MX" => "Mexico",
    "FM" => "Micronesia", "MI" => "Midway Islands", "MD" => "Moldova", "MC" => "Monaco", "MN" => "Mongolia",
    "ME" => "Montenegro", "MS" => "Montserrat", "MA" => "Morocco", "MZ" => "Mozambique", "MM" => "Myanmar [Burma]",
    "NA" => "Namibia", "NR" => "Nauru", "NP" => "Nepal", "NL" => "Netherlands", "AN" => "Netherlands Antilles",
    "NT" => "Neutral Zone", "NC" => "New Caledonia", "NZ" => "New Zealand", "NI" => "Nicaragua", "NE" => "Niger",
    "NG" => "Nigeria", "NU" => "Niue", "NF" => "Norfolk Island", "KP" => "North Korea", "VD" => "North Vietnam",
    "MP" => "Northern Mariana Islands", "NO" => "Norway", "OM" => "Oman", "PC" => "Pacific Islands Trust Territory",
    "PK" => "Pakistan", "PW" => "Palau", "PS" => "Palestinian Territories", "PA" => "Panama", "PZ" => "Panama Canal Zone",
    "PG" => "Papua New Guinea", "PY" => "Paraguay", "YD" => "People's Democratic Republic of Yemen", "PE" => "Peru",
    "PH" => "Philippines", "PN" => "Pitcairn Islands", "PL" => "Poland", "PT" => "Portugal", "PR" => "Puerto Rico",
    "QA" => "Qatar", "RO" => "Romania", "RU" => "Russia", "RW" => "Rwanda", "RE" => "Reunion", "BL" => "Saint Barthelemy",
    "SH" => "Saint Helena", "KN" => "Saint Kitts and Nevis", "LC" => "Saint Lucia", "MF" => "Saint Martin",
    "PM" => "Saint Pierre and Miquelon", "VC" => "Saint Vincent and the Grenadines", "WS" => "Samoa",
    "SM" => "San Marino", "SA" => "Saudi Arabia", "SN" => "Senegal", "RS" => "Serbia", "CS" => "Serbia and Montenegro",
    "SC" => "Seychelles", "SL" => "Sierra Leone", "SG" => "Singapore", "SK" => "Slovakia", "SI" => "Slovenia",
    "SB" => "Solomon Islands", "SO" => "Somalia", "ZA" => "South Africa", "GS" => "South Georgia and the South Sandwich Islands",
    "KR" => "South Korea", "ES" => "Spain", "LK" => "Sri Lanka", "SD" => "Sudan", "SR" => "Suriname",
    "SJ" => "Svalbard and Jan Mayen", "SZ" => "Swaziland", "SE" => "Sweden", "CH" => "Switzerland", "SY" => "Syria",
    "ST" => "Sao Tome and Principe", "TW" => "Taiwan", "TJ" => "Tajikistan", "TZ" => "Tanzania", "TH" => "Thailand",
    "TL" => "Timor-Leste", "TG" => "Togo", "TK" => "Tokelau", "TO" => "Tonga", "TT" => "Trinidad and Tobago",
    "TN" => "Tunisia", "TR" => "Turkey", "TM" => "Turkmenistan", "TC" => "Turks and Caicos Islands", "TV" => "Tuvalu",
    "UM" => "U.S. Minor Outlying Islands", "PU" => "U.S. Miscellaneous Pacific Islands", "VI" => "U.S. Virgin Islands",
    "UG" => "Uganda", "UA" => "Ukraine", "SU" => "Union of Soviet Socialist Republics", "AE" => "United Arab Emirates",
    "GB" => "United Kingdom", "US" => "United States", "ZZ" => "Unknown or Invalid Region", "UY" => "Uruguay",
    "UZ" => "Uzbekistan", "VU" => "Vanuatu", "VA" => "Vatican City", "VE" => "Venezuela", "VN" => "Vietnam",
    "WK" => "Wake Island", "WF" => "Wallis and Futuna", "EH" => "Western Sahara", "YE" => "Yemen", "ZM" => "Zambia",
    "ZW" => "Zimbabwe", "AX" => "Aland Islands",
);

/**
 * Get Country Name from Code
 */
function get_country_name($code='') {
    global $countries;
    foreach ($countries as $k => $v) {
        if ($code == $k) {
            $cname = $v;
        }
    }
    return $cname;
}

/**
 * Country Code Array [2 char]
 */
$country_codes = array(
    "AF", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AR", "AM", "AW", "AU", "AT", "AZ", "BS", "BH", "BD", "BB",
    "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "BQ", "IO", "VG", "BN", "BG", "BF", "BI", "KH",
    "CM", "CA", "CT", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "HR", "CU",
    "CY", "CZ", "CI", "DK", "DJ", "DM", "DO", "NQ", "DD", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ",
    "FI", "FR", "GF", "PF", "TF", "FQ", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG",
    "GN", "GW", "GY", "HT", "HM", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP",
    "JE", "JT", "JO", "KZ", "KE", "KI", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK",
    "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "FX", "MX", "FM", "MI", "MD", "MC", "MN", "ME",
    "MS", "MA", "MZ", "MM", "NA", "NR", "NP", "NL", "AN", "NT", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "KP", "VD",
    "MP", "NO", "OM", "PC", "PK", "PW", "PS", "PA", "PZ", "PG", "PY", "YD", "PE", "PH", "PN", "PL", "PT", "PR", "QA",
    "RO", "RU", "RW", "RE", "BL", "SH", "KN", "LC", "MF", "PM", "VC", "WS", "SM", "SA", "SN", "RS", "CS", "SC", "SL",
    "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "KR", "ES", "LK", "SD", "SR", "SJ", "SZ", "SE", "CH", "SY", "ST", "TW",
    "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV", "UM", "PU", "VI", "UG", "UA", "SU",
    "AE", "GB", "US", "ZZ", "UY", "UZ", "VU", "VA", "VE", "VN", "WK", "WF", "EH", "YE", "ZM", "ZW", "AX"
);

/**
 * Generate the State Dropdown.
 */
function state_dropdown($state='') {
    global $us_states;
    $state_dropdown = '<option value="">State</option>' . "\n";
    foreach ($us_states as $k => $v) {
        $state_dropdown .= '<option 
            value="' . $k . '"
            ' . ($state == $k ? ' selected' : '') . '>
            ' . $v . '
            </option>
        ' . "\n";
    }
    return $state_dropdown;
}

/**
 * Generate the Countries Dropdown.
 */
function countries_dropdown($country='') {
    global $countries;
    $country_dropdown = '<option value="">Countries</option>' . "\n";
    foreach ($countries as $k => $v) {
        $country_dropdown .= '<option 
            value="' . $k . '"
            ' . ($country == $k ? ' selected' : '') . '>
            ' . $v . '
            </option>
        ' . "\n";
    }
    return $country_dropdown;
}

/**
 * Add the orders front end shortcode function.
 */
function fn_gns_checkout() {
    global $current_user, $us_states, $us_state_codes, $countries, $country_codes, $_SESSION;

    if (isset($_GET['unset']) && $_GET['unset'] == 1) {
        unset($_SESSION['gns_checkout_cart_total']);
        unset($_SESSION['gns_checkout_cart_items']);
    }

    if (!isset($_SESSION['gns_checkout_cart_total'])) {
        $_SESSION['gns_checkout_cart_total'] = 0;
        $_SESSION['gns_checkout_cart_items'] = array();
    }

    $valid = true;
    $gns_checkout_msg = '';
    $gns_checkout_success = false;
    $html = '<div id="gns-checkout">';
    if (isset($_POST['gns_checkout_authorize'])) {
        if ($_POST['gns_checkout_billing_firstname'] != '') {
            $gns_checkout_billing_firstname = $_POST['gns_checkout_billing_firstname'];
        } else {
            $valid = false;
            $gns_checkout_billing_firstname_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing First Name is required</span><br />';
        }

        if ($_POST['gns_checkout_billing_lastname'] != '') {
            $gns_checkout_billing_lastname = $_POST['gns_checkout_billing_lastname'];
        } else {
            $valid = false;
            $gns_checkout_billing_lastname_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing Last Name is required</span><br />';
        }

        if (get_option('gns_checkout_include_billing_company') == 'yes') {
            if ($_POST['gns_checkout_billing_company'] != '') {
                $gns_checkout_billing_company = $_POST['gns_checkout_billing_company'];
            } else {
                $valid = false;
                $gns_checkout_billing_company_error = true;
                $gns_checkout_msg .= '<span>&#8226; Billing Company is required</span><br />';
            }
        }

        if ($_POST['gns_checkout_billing_address'] != '') {
            $gns_checkout_billing_address = $_POST['gns_checkout_billing_address'];
        } else {
            $valid = false;
            $gns_checkout_billing_address_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing Address is required</span><br />';
        }

        if ($_POST['gns_checkout_billing_city'] != '') {
            $gns_checkout_billing_city = $_POST['gns_checkout_billing_city'];
        } else {
            $valid = false;
            $gns_checkout_billing_city_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing City is required</span><br />';
        }

        if ($_POST['gns_checkout_billing_state'] != '') {
            if (!in_array($_POST['gns_checkout_billing_state'], $us_state_codes)) {
                $valid = false;
                $gns_checkout_billing_state_error = true;
                $gns_checkout_msg .= '<span>&#8226; Billing State format is invalid</span><br />';
            } else {
                $gns_checkout_billing_state = get_state_name($_POST['gns_checkout_billing_state']);
            }
        } else {
            $valid = false;
            $gns_checkout_billing_state_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing State is required</span><br />';
        }

        if ($_POST['gns_checkout_billing_zip'] != '') {
            if (validate_zipcode($_POST['gns_checkout_billing_zip'])) {
                $gns_checkout_billing_zip = $_POST['gns_checkout_billing_zip'];
            } else {
                $valid = false;
                $gns_checkout_billing_zip_error = true;
                $gns_checkout_msg .= '<span>&#8226; Billing Zip Code format is invalid.</span><br />';
            }
        } else {
            $valid = false;
            $gns_checkout_billing_zip_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing Zip Code is required</span><br />';
        }

        if ($_POST['gns_checkout_billing_country'] != '') {
            if (!in_array($_POST['gns_checkout_billing_country'], $country_codes)) {
                $valid = false;
                $gns_checkout_billing_country_error = true;
                $gns_checkout_msg .= '<span>&#8226; Billing Country format is invalid</span><br />';
            } else {
                $gns_checkout_billing_country = get_country_name($_POST['gns_checkout_billing_country']);
            }
        } else {
            $valid = false;
            $gns_checkout_billing_country_error = true;
            $gns_checkout_msg .= '<span>&#8226; Billing Country is required</span><br />';
        }

        if (get_option('gns_checkout_include_shipping') == 'yes') {
            if ($_POST['gns_checkout_shipping_firstname'] != '') {
                $gns_checkout_shipping_firstname = $_POST['gns_checkout_shipping_firstname'];
            } else {
                $valid = false;
                $gns_checkout_shipping_firstname_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping First Name is required</span><br />';
            }

            if ($_POST['gns_checkout_shipping_lastname'] != '') {
                $gns_checkout_shipping_lastname = $_POST['gns_checkout_shipping_lastname'];
            } else {
                $valid = false;
                $gns_checkout_shipping_lastname_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping Last Name is required</span><br />';
            }

            if (get_option('gns_checkout_include_shipping_company') == 'yes') {
                if ($_POST['gns_checkout_shipping_company'] != '') {
                    $gns_checkout_shipping_company = $_POST['gns_checkout_shipping_company'];
                } else {
                    $valid = false;
                    $gns_checkout_shipping_company_error = true;
                    $gns_checkout_msg .= '<span>&#8226; Shipping Company is required</span><br />';
                }
            }

            if ($_POST['gns_checkout_shipping_address'] != '') {
                $gns_checkout_shipping_address = $_POST['gns_checkout_shipping_address'];
            } else {
                $valid = false;
                $gns_checkout_shipping_address_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping Address is required</span><br />';
            }

            if ($_POST['gns_checkout_shipping_city'] != '') {
                $gns_checkout_shipping_city = $_POST['gns_checkout_shipping_city'];
            } else {
                $valid = false;
                $gns_checkout_shipping_city_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping City is required</span><br />';
            }

            if ($_POST['gns_checkout_shipping_state'] != '') {
                if (!in_array($_POST['gns_checkout_shipping_state'], $us_state_codes)) {
                    $valid = false;
                    $gns_checkout_shipping_state_error = true;
                    $gns_checkout_msg .= '<span>&#8226; Shipping State format is invalid</span><br />';
                } else {
                    $gns_checkout_shipping_state = get_state_name($_POST['gns_checkout_shipping_state']);
                }
            } else {
                $valid = false;
                $gns_checkout_shipping_state_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping State is required</span><br />';
            }

            if ($_POST['gns_checkout_shipping_zip'] != '') {
                if (validate_zipcode($_POST['gns_checkout_shipping_zip'])) {
                    $gns_checkout_shipping_zip = $_POST['gns_checkout_shipping_zip'];
                } else {
                    $valid = false;
                    $gns_checkout_shipping_zip_error = true;
                    $gns_checkout_msg .= '<span>&#8226; Shipping Zip Code format is invalid.</span><br />';
                }
            } else {
                $valid = false;
                $gns_checkout_shipping_zip_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping Zip Code is required</span><br />';
            }

            if ($_POST['gns_checkout_shipping_country'] != '') {
                if (!in_array($_POST['gns_checkout_shipping_country'], $country_codes)) {
                    $valid = false;
                    $gns_checkout_shipping_country_error = true;
                    $gns_checkout_msg .= '<span>&#8226; Shipping Country format is invalid</span><br />';
                } else {
                    $gns_checkout_shipping_country = get_country_name($_POST['gns_checkout_shipping_country']);
                }
            } else {
                $valid = false;
                $gns_checkout_shipping_country_error = true;
                $gns_checkout_msg .= '<span>&#8226; Shipping Country is required</span><br />';
            }
        }

        if (get_option('gns_checkout_include_optional') == 'yes') {
            if ($_POST['gns_checkout_phone'] != '') {
                $gns_checkout_phone = $_POST['gns_checkout_phone'];
            } else {
                $valid = false;
                $gns_checkout_phone_error = true;
                $gns_checkout_msg .= '<span>&#8226; Phone Number is required</span><br />';
            }

            if ($_POST['gns_checkout_email'] != '') {
                $gns_checkout_email = $_POST['gns_checkout_email'];
                if (preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $gns_checkout_email)) {
                } else {
                    $valid = false;
                    $gns_checkout_email_error = true;
                    $gns_checkout_msg .= '<span>&#8226; Invalid email format</span><br />';
                }
            } else {
                $valid = false;
                $gns_checkout_email_error = true;
                $gns_checkout_msg .= '<span>&#8226; E-mail is required</span><br />';
            }

            if ($_POST['gns_checkout_po_invoice'] != '') {
                $gns_checkout_po_invoice = $_POST['gns_checkout_po_invoice'];
            } else {
                $valid = false;
                $gns_checkout_po_invoice_error = true;
                $gns_checkout_msg .= '<span>&#8226; PO/Invoice is required</span><br />';
            }
        }

        if ($_POST['gns_checkout_amount'] != '') {
            $gns_checkout_amount = $_POST['gns_checkout_amount'];
            if (
                (is_numeric($gns_checkout_amount)) &&
                ((strlen($gns_checkout_amount) > '1') ||
                    (strlen($gns_checkout_amount) == '1')
                )
            ) {
            } else {
                $valid = false;
                $gns_checkout_amount_error = true;
                $gns_checkout_msg .= '<span>&#8226; Amount cannot be less then $1.00</span><br />';
            }
        } else {
            $valid = false;
            $gns_checkout_amount_error = true;
            $gns_checkout_msg .= '<span>&#8226; Amount is required</span><br />';
        }

        if ($_POST['gns_checkout_card_number'] != '') {
            $gns_checkout_card_number = $_POST['gns_checkout_card_number'];
            if ((is_numeric($gns_checkout_card_number)) && (strlen($gns_checkout_card_number) > '15')) {
            } else {
                $valid = false;
                $gns_checkout_card_number_error = true;
                $gns_checkout_msg .= '<span>&#8226; Please enter a valid Card Number</span><br />';
            }
        } else {
            $valid = false;
            $gns_checkout_card_number_error = true;
            $gns_checkout_msg .= '<span>&#8226; Card Number is required</span><br />';
        }

        if ($_POST['gns_checkout_cvv'] != '') {
            $gns_checkout_cvv = $_POST['gns_checkout_cvv'];
            if ((is_numeric($gns_checkout_cvv)) && (strlen($gns_checkout_cvv) == '3')) {
            } else {
                $valid = false;
                $gns_checkout_cvv_error = true;
                $gns_checkout_msg .= '<span>&#8226; Please enter a valid CVV</span><br />';
            }
        } else {
            $valid = false;
            $gns_checkout_cvv_error = true;
            $gns_checkout_msg .= '<span>&#8226; CVV is required</span><br />';
        }

        if ($_POST['gns_checkout_card_expiration'] != '') {
            $gns_checkout_card_expiration = $_POST['gns_checkout_card_expiration'];
            if ((is_numeric($gns_checkout_card_expiration)) && (strlen($gns_checkout_card_expiration) == '4')) {
            } else {
                $valid = false;
                $gns_checkout_card_expiration_error = true;
                $gns_checkout_msg .= '<span>&#8226; Please enter a valid Card Expiration Date</span><br />';
            }
        } else {
            $valid = false;
            $gns_checkout_card_expiration_error = true;
            $gns_checkout_msg .= '<span>&#8226; Card Expiration Date is required</span><br />';
        }

        if ($valid) {
            $gns_checkout_billing_firstname;
            $gns_checkout_billing_lastname;
            $gns_checkout_billing_company;
            $gns_checkout_billing_address;
            $gns_checkout_billing_city;
            $gns_checkout_billing_state;
            $gns_checkout_billing_zip;
            $gns_checkout_billing_country;
            $gns_checkout_shipping_firstname;
            $gns_checkout_shipping_lastname;
            $gns_checkout_shipping_company;
            $gns_checkout_shipping_address;
            $gns_checkout_shipping_city;
            $gns_checkout_shipping_state;
            $gns_checkout_shipping_zip;
            $gns_checkout_shipping_country;
            $gns_checkout_phone;
            $gns_checkout_email;
            $gns_checkout_po_invoice;
            $gns_checkout_amount;
            $gns_checkout_card_number;
            $gns_checkout_cvv;
            $gns_checkout_card_expiration;
            $gns_checkout_auth_login_id = get_option('gns_checkout_auth_login_id');
            $gns_checkout_auth_transaction_key = get_option('gns_checkout_auth_transaction_key');
            $gns_checkout_processor_description = get_option('gns_checkout_processor_description');
            $gns_checkout_auth_mode = get_option('gns_checkout_auth_mode');
            //$gns_checkout_flag_admin = get_option('gns_checkout_admin_notification');
            //$gns_checkout_flag_checkout = get_option('gns_checkout_notification');
            //$gns_checkout_fax;
            //$gns_checkout_cust_id;
            //$gns_checkout_customer_ip;
            //$x_tax;
            //$x_po_num;

            if ($gns_checkout_auth_mode == "live") {
                $post_url = "https://secure.authorize.net/gateway/transact.dll";
            } else {
                $post_url = "https://test.authorize.net/gateway/transact.dll";
            }

            $post_values = array(
                "x_login" => $gns_checkout_auth_login_id,
                "x_tran_key" => $gns_checkout_auth_transaction_key,
                "x_version" => "3.1",
                "x_type" => "AUTH_CAPTURE",
                "x_method" => "CC",
                "x_amount" => $gns_checkout_amount,
                "x_currency_code" => 'USD',
                "x_card_num" => $gns_checkout_card_number,
                "x_exp_date" => $gns_checkout_card_expiration,
                "x_card_code" => $gns_checkout_cvv,
                "x_description" => $gns_checkout_processor_description,
                "x_delim_data" => "TRUE",
                "x_delim_char" => "|",
                "x_relay_response" => "FALSE",
                "x_first_name" => $gns_checkout_billing_firstname,
                "x_last_name" => $gns_checkout_billing_lastname,
                "x_company" => $gns_checkout_billing_company,
                "x_address" => $gns_checkout_billing_address,
                "x_city" => $gns_checkout_billing_city,
                "x_state" => $gns_checkout_billing_state,
                "x_zip" => $gns_checkout_billing_zip,
                "x_country" => $gns_checkout_billing_country,
                "x_ship_to_first_name" => $gns_checkout_shipping_firstname,
                "x_ship_to_last_name" => $gns_checkout_shipping_lastname,
                "x_ship_to_company" => $gns_checkout_shipping_company,
                "x_ship_to_address" => $gns_checkout_shipping_address,
                "x_ship_to_city" => $gns_checkout_shipping_city,
                "x_ship_to_state" => $gns_checkout_shipping_state,
                "x_ship_to_zip" => $gns_checkout_shipping_zip,
                "x_ship_to_country" => $gns_checkout_shipping_country,
                "x_phone" => $gns_checkout_phone,
                "x_email" => $gns_checkout_email,
                "x_po_num" => $gns_checkout_po_invoice,
                //"x_fax" => $gns_checkout_fax,
                //"x_cust_id" => $gns_checkout_cust_id,
                //"x_customer_ip" => $gns_checkout_customer_ip,
                //"x_allow_partial_Auth" => false,
                //"x_response_format" => 2, // Overrides default response
                //"x_market_type" => 0, // 0 e-commerce, 1 moto, 2 retail
                //"x_recurring_billing" => 0,
                //"x_trans_id" => 12345, // originally assigned transaction id
                //"x_invoice_num" => 12345, // optional, merchant-assigned invoice number
                //"x_tax" => $gns_checkout_tax,
            );

            $post_string = "";
            foreach ($post_values as $key => $value) {
                $post_string .= "$key=" . urlencode($value) . "&";
            }
            $post_string = rtrim($post_string, "& ");

            // Connection to Authorize.net
            $request = curl_init($post_url);
            curl_setopt($request, CURLOPT_HEADER, 0);
            curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($request, CURLOPT_POSTFIELDS, $post_string);
            curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
            $post_response = curl_exec($request);
            curl_close($request);

            // Response from Authorize.net
            $response_array = explode($post_values["x_delim_char"], $post_response);
            $gns_checkout_transaction_id = $response_array[6];
            $gns_checkout_last_4 = $response_array[50];
            $gns_checkout_approval_code = $response_array[4];

            if ($response_array[0] == '1') {
                $post = array(
                    'post_type' => 'gns_checkout',
                    'post_title' => sanitize_text_field($_POST['gns_checkout_billing_firstname']) . ' ' .
                        sanitize_text_field($_POST['gns_checkout_billing_lastname']),
                    'post_status' => 'publish',
                    'post_author' => 1,
                );
                $post_id = wp_insert_post($post);
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_firstname',
                    sanitize_text_field($_POST['gns_checkout_billing_firstname']),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_lastname',
                    sanitize_text_field($_POST['gns_checkout_billing_lastname']),
                    true
                );
                if (get_option('gns_checkout_include_billing_company') == 'yes') {
                    add_post_meta(
                        $post_id,
                        'gns_checkout_billing_company',
                        sanitize_text_field($_POST['gns_checkout_billing_company']),
                        true
                    );
                }
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_address',
                    sanitize_text_field($_POST['gns_checkout_billing_address']),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_city',
                    sanitize_text_field($_POST['gns_checkout_billing_city']),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_state',
                    sanitize_text_field(get_state_name($_POST['gns_checkout_billing_state'])),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_zip',
                    sanitize_text_field($_POST['gns_checkout_billing_zip']),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_billing_country',
                    sanitize_text_field(get_country_name($_POST['gns_checkout_billing_country'])),
                    true
                );
                if (get_option('gns_checkout_include_shipping') == 'yes') {
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_firstname',
                        sanitize_text_field($_POST['gns_checkout_shipping_firstname']),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_lastname',
                        sanitize_text_field($_POST['gns_checkout_shipping_lastname']),
                        true
                    );
                    if (get_option('gns_checkout_include_shipping_company') == 'yes') {
                        add_post_meta(
                            $post_id,
                            'gns_checkout_shipping_company',
                            sanitize_text_field($_POST['gns_checkout_shipping_company']),
                            true
                        );
                    }
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_address',
                        sanitize_text_field($_POST['gns_checkout_shipping_address']),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_city',
                        sanitize_text_field($_POST['gns_checkout_shipping_city']),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_state',
                        sanitize_text_field(get_state_name($_POST['gns_checkout_shipping_state'])),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_zip',
                        sanitize_text_field($_POST['gns_checkout_shipping_zip']),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_shipping_country',
                        sanitize_text_field(get_country_name($_POST['gns_checkout_shipping_country'])),
                        true
                    );
                }
                add_post_meta(
                    $post_id,
                    'gns_checkout_amount',
                    sanitize_text_field($_POST['gns_checkout_amount']),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_transaction_id',
                    sanitize_text_field($gns_checkout_transaction_id),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_last_4',
                    sanitize_text_field($gns_checkout_last_4),
                    true
                );
                add_post_meta(
                    $post_id,
                    'gns_checkout_approval_code',
                    sanitize_text_field($gns_checkout_approval_code),
                    true
                );
                if (get_option('gns_checkout_include_optional') == 'yes') {
                    add_post_meta(
                        $post_id,
                        'gns_checkout_phone',
                        sanitize_text_field($_POST['gns_checkout_phone']),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_email',
                        sanitize_text_field($_POST['gns_checkout_email']),
                        true
                    );
                    add_post_meta(
                        $post_id,
                        'gns_checkout_po_invoice',
                        sanitize_text_field($_POST['gns_checkout_po_invoice']),
                        true
                    );
                }
                ///// CUSTOMIZE/ADD TO THIS ARRAY TO STORE CART ITEMS IN DATABASE ////////////////////////
                // added order products info to order meta
                /*if (!empty($_SESSION['gns_checkout_cart_items'])) {
                    foreach ($_SESSION['gns_checkout_cart_items'] as $i => $gns_checkout_cart_item) {
                        add_post_meta(
                            $post_id,
                            'gns_checkout_cart_item_' . $i . '_item_id',
                            sanitize_text_field($gns_checkout_cart_item['item_id']),
                            true
                        );
                        add_post_meta(
                            $post_id,
                            'gns_checkout_cart_item_' . $i . '_item_xxxx',
                            sanitize_text_field($gns_checkout_cart_item['item_xxxx']),
                            true
                        );
                    }
                }*/
                ///// CUSTOMIZE/ADD TO THIS ARRAY TO STORE CART ITEMS IN DATABASE ////////////////////////
                unset($_SESSION['gns_checkout_cart_items']);
                unset($_SESSION['gns_checkout_cart_total']);
                $gns_checkout_success = true;
            } else if ($response_array[0] == '2') {
                $gns_checkout_msg .= '<p>' . $response_array[3] . '<p>';
            } else {
                $gns_checkout_msg .= '<p>' . $response_array[3] . '</p>';
            }
        } else {
            // nothing here yet
        }
    }

    if (!$valid) {
        $html .= '<div class="checkout-message">' .
            ($gns_checkout_msg != '' ?
                $gns_checkout_msg : 'You have errors in your submission. Check the form and try again.')
            . '</div>';
    }

    if ($gns_checkout_success) {
        $html .= '<div class="checkout-success">' . get_option('gns_checkout_thank_you_message') . '</div>';
    } else {
        if ($valid) {
            if (
            (isset($_POST['gns_checkout_cart_total']) &&
                $_POST['gns_checkout_cart_total'] > 0)
            ) {
                if (isset($_SESSION['gns_checkout_cart_total'])) {
                    $_SESSION['gns_checkout_cart_total'] =
                        ($_SESSION['gns_checkout_cart_total'] + $_POST['gns_checkout_cart_total']);
                } else {
                    $_SESSION['gns_checkout_cart_total'] = $_POST['gns_checkout_cart_total'];
                }
            }
        }
        $html .= '
            <div class="checkout-form">            
                <form method="post" enctype="multipart/form-data">
                    <!--<div class="checkout-items">
                        <h3>Checkout Items</h3>
                        <hr />
                        Items List
                        <hr />
                    </div>-->
                    <div class="checkout-total">
                        <label>Total: $' . number_format($_SESSION['gns_checkout_cart_total'], 2, ".", ",") . '</label>
                        <input
                            type="hidden"
                            value="' . (($_SESSION['gns_checkout_cart_total'] > 1) ?
                            $_SESSION['gns_checkout_cart_total'] : 1) . '"
                            name="gns_checkout_amount"
                            id="gns_checkout_amount">
                    </div>
                    <div class="checkout-required">
                        <p>All fields required.</p>
                    </div>                        
                    <div class="checkout-billing gnsEqualheight">
                        <h3>Billing Information</h3>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_billing_firstname'] . '"
                                placeholder="Billing First Name"
                                name="gns_checkout_billing_firstname"
                                id="gns_checkout_billing_firstname"
                                ' . (($gns_checkout_billing_firstname_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_billing_lastname'] . '"
                                placeholder="Billing Last Name"
                                name="gns_checkout_billing_lastname"
                                id="gns_checkout_billing_lastname"
                                ' . (($gns_checkout_billing_lastname_error) ? 'class="checkout-error"' : '') . '>
                        </div>';
        if (get_option('gns_checkout_include_billing_company') == 'yes') {
            $html   .= '<div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_billing_company'] . '"
                                placeholder="Billing Company Name"
                                name="gns_checkout_billing_company"
                                id="gns_checkout_billing_company"
                                ' . (($gns_checkout_billing_company_error) ? 'class="checkout-error"' : '') . '>
                        </div>';
        }
            $html   .= '<div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_billing_address'] . '"
                                placeholder="Billing Address"
                                name="gns_checkout_billing_address"
                                id="gns_checkout_billing_address"
                                ' . (($gns_checkout_billing_address_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_billing_city'] . '"
                                placeholder="Billing City"
                                name="gns_checkout_billing_city"
                                id="gns_checkout_billing_city"
                                ' . (($gns_checkout_billing_city_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input billing-state">
                            <select name="gns_checkout_billing_state" id="gns_checkout_billing_state"
                                ' . (($gns_checkout_billing_state_error) ? 'class="checkout-error"' : '') . '>
                                ' . state_dropdown($_POST['gns_checkout_billing_state']) . '
                            </select>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_billing_zip'] . '"
                                placeholder="Billing Zipcode"
                                name="gns_checkout_billing_zip"
                                id="gns_checkout_billing_zip"
                                ' . (($gns_checkout_billing_zip_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <select 
                                name="gns_checkout_billing_country" 
                                id="gns_checkout_billing_country"
                                onchange="checkCountryCode(this.value, \'billing\');"
                                ' . (($gns_checkout_billing_country_error) ? 'class="checkout-error"' : '') . '>
                                ' . countries_dropdown(
                                    (
                                        ($_POST['gns_checkout_billing_country']) ?
                                            $_POST['gns_checkout_billing_country'] : 'US'
                                    )
                                ) . '
                            </select>
                        </div>
                    </div>';
        if (get_option('gns_checkout_include_shipping') == 'yes') {
        $html   .= '<div class="checkout-shipping gnsEqualheight">
                        <h3>Shipping Information</h3>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_shipping_firstname'] . '"
                                placeholder="Shipping First Name"
                                name="gns_checkout_shipping_firstname"
                                id="gns_checkout_shipping_firstname"
                                ' . (($gns_checkout_shipping_firstname_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_shipping_lastname'] . '"
                                placeholder="Shipping Last Name"
                                name="gns_checkout_shipping_lastname"
                                id="gns_checkout_shipping_lastname"
                                ' . (($gns_checkout_shipping_lastname_error) ? 'class="checkout-error"' : '') . '>
                        </div>';
        if (get_option('gns_checkout_include_shipping_company') == 'yes') {
            $html   .= '<div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_shipping_company'] . '"
                                placeholder="Shipping Company Name"
                                name="gns_checkout_shipping_company"
                                id="gns_checkout_shipping_company"
                                ' . (($gns_checkout_shipping_company_error) ? 'class="checkout-error"' : '') . '>
                        </div>';
        }
            $html   .= '<div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_shipping_address'] . '"
                                placeholder="Shipping Address"
                                name="gns_checkout_shipping_address"
                                id="gns_checkout_shipping_address"
                                ' . (($gns_checkout_shipping_address_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_shipping_city'] . '"
                                placeholder="Shipping City"
                                name="gns_checkout_shipping_city"
                                id="gns_checkout_shipping_city"
                                ' . (($gns_checkout_shipping_city_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input shipping-state">
                            <select name="gns_checkout_shipping_state" id="gns_checkout_shipping_state"
                                ' . (($gns_checkout_shipping_state_error) ? 'class="checkout-error"' : '') . '>
                                ' . state_dropdown($_POST['gns_checkout_shipping_state']) . '
                            </select>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_shipping_zip'] . '"
                                placeholder="Shipping Zipcode"
                                name="gns_checkout_shipping_zip"
                                id="gns_checkout_shipping_zip"
                                ' . (($gns_checkout_shipping_zip_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <select 
                                name="gns_checkout_shipping_country" 
                                id="gns_checkout_shipping_country"
                                onchange="checkCountryCode(this.value, \'shipping\');"
                                ' . (($gns_checkout_shipping_country_error) ? 'class="checkout-error"' : '') . '>
                                ' . countries_dropdown(
                                    (
                                    ($_POST['gns_checkout_shipping_country']) ?
                                        $_POST['gns_checkout_shipping_country'] : 'US'
                                    )
                                ) . '
                            </select>
                        </div>
                    </div>';
        }
        if (get_option('gns_checkout_include_optional') == 'yes') {
        $html   .= '<div class="checkout-optional gnsEqualheight">
                        <h3>Optional Information</h3>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_phone'] . '"
                                placeholder="Phone Number"
                                name="gns_checkout_phone"
                                id="gns_checkout_phone"
                                ' . (($gns_checkout_phone_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_email'] . '"
                                placeholder="E-mail"
                                name="gns_checkout_email"
                                id="gns_checkout_email"
                                ' . (($gns_checkout_email_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                value="' . $_POST['gns_checkout_po_invoice'] . '"
                                placeholder="PO/Invoice"
                                name="gns_checkout_po_invoice"
                                id="gns_checkout_po_invoice"
                                ' . (($gns_checkout_po_invoice_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                    </div>';
        }
        $html   .= '<div class="checkout-payment gnsEqualheight">
                        <h3>Payment Information</h3>
                        <div class="form-input">
                            <input
                                type="text"
                                maxlength="19"
                                value="' . $_POST['gns_checkout_card_number'] . '"
                                placeholder="Card Number"
                                name="gns_checkout_card_number"
                                id="gns_checkout_card_number"
                                ' . (($gns_checkout_card_number_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                maxlength="3"
                                value="' . $_POST['gns_checkout_cvv'] . '"
                                placeholder="CVV"
                                name="gns_checkout_cvv"
                                id="gns_checkout_cvv"
                                ' . (($gns_checkout_cvv_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                        <div class="form-input">
                            <input
                                type="text"
                                maxlength="4"
                                value="' . $_POST['gns_checkout_card_expiration'] . '"
                                placeholder="Card Expiration: (mmyy)"
                                name="gns_checkout_card_expiration"
                                id="gns_checkout_card_expiration"
                                ' . (($gns_checkout_card_expiration_error) ? 'class="checkout-error"' : '') . '>
                        </div>
                    </div>';
        $html   .= '<div class="checkout-submit">
                        <div class="form-input">
                            <input
                                type="submit"
                                name="gns_checkout_authorize" 
                                value="Checkout Now">
                        </div>
                    </div>
                </form>
            </div>';
    }
    $html .= '</div>';
    return $html;
}
add_shortcode('gns_authorize_checkout', 'fn_gns_checkout');

/**
 * Add the orders admin settings page.
 */
function gns_checkout_register_settings() {
    register_setting('gns-settings-group', 'gns_checkout_auth_login_id');
    register_setting('gns-settings-group', 'gns_checkout_auth_transaction_key');
    register_setting('gns-settings-group', 'gns_checkout_auth_mode');
    register_setting('gns-settings-group', 'gns_checkout_include_shipping');
    register_setting('gns-settings-group', 'gns_checkout_include_billing_company');
    register_setting('gns-settings-group', 'gns_checkout_include_shipping_company');
    register_setting('gns-settings-group', 'gns_checkout_include_optional');
    register_setting('gns-settings-group', 'gns_checkout_thank_you_message');
    register_setting('gns-settings-group', 'gns_checkout_processor_description');
}
function gns_checkout_settings_page() {
    ?>
    <div class="wrap">
        <h2>Authorize.Net Simple Checkout</h2>
        <?php
        $tabs = array(
            'settings' => 'Settings',
            'usage'  => 'Usage',
        );
        gns_admin_tabs('gns-checkout-settings-page', $tabs, 'settings');
        ?>
    </div>
    <?php
}
function gns_checkout_create_menu() {
    add_menu_page(
        'Authorize.Net',
        'Authorize.Net',
        'administrator',
        'gns-checkout-settings-page',
        'gns_checkout_settings_page'
    );
    add_action('admin_init', 'gns_checkout_register_settings');
}
add_action('admin_menu', 'gns_checkout_create_menu');

/**
 * Create admin tabs
 */
function gns_admin_tabs($page, $tabs, $current = NULL) {
    if (isset($_GET['tab'])) {
        //if (is_null($current)) {
        $current = $_GET['tab'];
        //}
    }
    $content = '';
    $content .= '<h2 class="nav-tab-wrapper">';
    $cnt=0;
    foreach($tabs as $tab => $tabname) {
        if ($current == $tab || (empty($current) && $cnt == 0)) {
            $class = ' nav-tab-active';
        } else {
            $class = '';
        }
        $content .= '<a class="nav-tab' . $class . '" href="?page=' .
            $page . '&tab=' . $tab . '">' . $tabname . '</a>';
        $cnt++;
    }
    $content .= '</h2>';
    echo $content;
    if (!$current)
        $current = key($tabs);
    include_once('tabs/' . $current . '.php');
    return;
}

/**
 * Hide the orders submenu's they are not needed.
 */
function gns_checkout_remove_order_submenus() {
    global $submenu;
    unset($submenu['edit.php?post_type=gns_checkout'][5]);
    unset($submenu['edit.php?post_type=gns_checkout'][10]);
}
add_action('admin_init', 'gns_checkout_remove_order_submenus');

/**
 * Hide the orders view link in listing.
 */
function gns_checkout_remove_row_actions($actions) {
    if (get_post_type() == 'gns_checkout')
        unset($actions['view']);
        unset($actions['inline hide-if-no-js']);
    return $actions;
}
add_filter('post_row_actions', 'gns_checkout_remove_row_actions', 10, 1);

/**
 * Change the orders post row edit to view text.
 */
function gns_checkout_modify_orders_row_actions($actions, $post) {
    // Check for orders post type
    if ($post->post_type == "gns_checkout") {
        // Save default $actions
        $url = admin_url('post.php?post=' . $post->ID . '&action=edit');
        $edit = add_query_arg(array('action' => 'edit'), $url);
        $trash = $actions['trash'];
        // Reset the default $actions with your own array
        $actions = array(
            'edit' => sprintf(
                '<a href="%1$s">%2$s</a>',
                esc_url($edit),
                esc_html(__('View', 'textdomain'))
            )
        );
        $actions['trash'] = $trash;
    }
    return $actions;
}
add_filter('post_row_actions', 'gns_checkout_modify_orders_row_actions', 10, 2);

/**
 * Add the orders edit admin page meta box.
 */
function gns_checkout_meta_box_callback($post) {
    ?>
    <ul>
        <h3>Authorize.Net Information</h3>
        <li>
            <div class="html-label">Total</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_amount"
                    id="gns_checkout_amount" disabled
                    value="<?php echo number_format(esc_attr(get_post_meta($post->ID, 'gns_checkout_amount', true)), 2, ".", ","); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Transaction ID</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_transaction_id"
                    id="gns_checkout_transaction_id" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_transaction_id', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Last 4</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_last_4"
                    id="gns_checkout_last_4" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_last_4', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Approval Code</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_approval_code"
                    id="gns_checkout_approval_code" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_approval_code', true)); ?>">
            </div>
        </li>
    </ul>
    <ul>
        <h3>Billing Information</h3>
        <li>
            <div class="html-label">First Name</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_firstname"
                    id="gns_checkout_billing_firstname" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_firstname', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Last Name</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_lastname"
                    id="gns_checkout_billing_lastname" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_lastname', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Company Name</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_company"
                    id="gns_checkout_billing_company" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_company', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Billing Address</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_address"
                    id="gns_checkout_billing_address" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_address', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Billing City</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_city"
                    id="gns_checkout_billing_city" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_city', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Billing State</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_state"
                    id="gns_checkout_billing_state" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_state', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Billing Zip</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_zip"
                    id="gns_checkout_billing_zip" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_zip', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Billing Country</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_billing_country"
                    id="gns_checkout_billing_country" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_billing_country', true)); ?>">
            </div>
        </li>
    </ul>
    <ul>
        <h3>Shipping Information</h3>
        <li>
            <div class="html-label">Shipping First Name</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_firstname"
                    id="gns_checkout_shipping_firstname" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_firstname', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping Last Name</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_lastname"
                    id="gns_checkout_shipping_lastname" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_lastname', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping Company</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_company"
                    id="gns_checkout_shipping_company" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_company', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping Address</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_address"
                    id="gns_checkout_shipping_address" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_address', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping City</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_city"
                    id="gns_checkout_shipping_city" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_city', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping State</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_state"
                    id="gns_checkout_shipping_state" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_state', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping Zip</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_zip"
                    id="gns_checkout_shipping_zip" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_zip', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Shipping Country</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_shipping_country"
                    id="gns_checkout_shipping_country" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_shipping_country', true)); ?>">
            </div>
        </li>
    </ul>
    <ul>
        <h3>Optional Information</h3>
        <li>
            <div class="html-label">E-mail</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_email"
                    id="gns_checkout_email" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_email', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">Phone</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_phone"
                    id="gns_checkout_phone" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_phone', true)); ?>">
            </div>
        </li>
        <li>
            <div class="html-label">PO/Invoice</div>
            <div class="html-field form-group">
                <input
                    type="text"
                    name="gns_checkout_po_invoice"
                    id="gns_checkout_po_invoice" disabled
                    value="<?php echo esc_attr(get_post_meta($post->ID, 'gns_checkout_po_invoice', true)); ?>">
            </div>
        </li>
    </ul>
    <div style="clear:both;"></div>
    <?php
}
function gns_checkout_add_meta_box() {
    $screens = array('gns_checkout');
    foreach ($screens as $screen) {
        add_meta_box(
            'gns_checkout_sectionid',
            __('Order Information',
                'gns_checkout_textdomain'),
            'gns_checkout_meta_box_callback',
            $screen,
            'normal',
            'high'
        );
    }
}
add_action('add_meta_boxes', 'gns_checkout_add_meta_box');

/**
 * Update the view of the orders listing column headings.
 */
add_filter('manage_edit-gns_checkout_columns', 'edit_gns_checkout_columns');
function edit_gns_checkout_columns($columns) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => __('Title'),
        'gns_checkout_email' => __('Email'),
        'gns_checkout_amount' => __('Total'),
        'gns_checkout_transaction_id' => __('Transaction ID'),
        'gns_checkout_approval_code' => __('Approval Code'),
        'date' => __('Order Date')
    );
    return $columns;
}

/**
 * Update the view of the events listing column content.
 */
add_action('manage_gns_checkout_posts_custom_column', 'manage_gns_checkout_columns', 10, 2);
function manage_gns_checkout_columns($column, $post_id) {
    global $post;

    switch($column) {
        case 'gns_checkout_title':
            $gns_checkout_firstname = get_post_meta($post_id, 'gns_checkout_firstname', true);
            $gns_checkout_lastname = get_post_meta($post_id, 'gns_checkout_lastname', true);
            if (empty($gns_checkout_firstname))
                echo __('--');
            else
                echo $gns_checkout_firstname . ' ' . $gns_checkout_lastname;
            break;
        case 'gns_checkout_email':
            $gns_checkout_email = get_post_meta($post_id, 'gns_checkout_email', true);
            if (empty($gns_checkout_email))
                echo __('--');
            else
                echo $gns_checkout_email;
            break;
        case 'gns_checkout_amount':
            $gns_checkout_amount = get_post_meta($post_id, 'gns_checkout_amount', true);
            if (empty($gns_checkout_amount))
                echo __('$0.00');
            else
                echo '$' . number_format($gns_checkout_amount, 2, ".", ",");
            break;
        case 'gns_checkout_transaction_id':
            $gns_checkout_transaction_id = get_post_meta($post_id, 'gns_checkout_transaction_id', true);
            if (empty($gns_checkout_transaction_id))
                echo __('--');
            else
                echo $gns_checkout_transaction_id;
            break;
        case 'gns_checkout_approval_code':
            $gns_checkout_approval_code = get_post_meta($post_id, 'gns_checkout_approval_code', true);
            if (empty($gns_checkout_approval_code))
                echo __('--');
            else
                echo $gns_checkout_approval_code;
            break;
        default :
            break;
    }
}

/**
 * Update the view of the orders edit admin pages.
 */
function gns_checkout_custom_admin_post_css() {
    global $post_type;

    if ($post_type == 'gns_checkout') {
        echo "
            <style>
                #edit-slug-box { display:none; }
                #submitdiv { display:none; }
                #postbox-container-1 { display:none; }
                #poststuff #post-body.columns-2 { margin-right:0; }
            </style>
        ";
    }
}
add_action('admin_head', 'gns_checkout_custom_admin_post_css');

/**
 * Validate Zip Code (US)
 */
function validate_zipcode($zipcode='') {
    return preg_match('/^[0-9]{5}([- ]?[0-9]{4})?$/', $zipcode);
}

/**
 * Runs when plugin is activated
 */
function gns_checkout_install() {
    global $wpdb;

    $the_page_title = 'Authorize.Net Simple Checkout';
    $the_page_name = 'simple-checkout';

    // the menu entry...
    delete_option("gns_checkout_page_title");
    add_option("gns_checkout_page_title", $the_page_title, '', 'yes');

    // the slug...
    delete_option("gns_checkout_page_name");
    add_option("gns_checkout_page_name", $the_page_name, '', 'yes');

    // the id...
    delete_option("gns_checkout_page_id");
    add_option("gns_checkout_page_id", '0', '', 'yes');

    $the_page = get_page_by_title($the_page_title);

    if (!$the_page) {
        // Create post object
        $_p = array();
        $_p['post_title'] = $the_page_title;
        $_p['post_name'] = $the_page_name;
        $_p['post_content'] = "[gns_authorize_checkout]";
        $_p['post_status'] = 'publish';
        $_p['post_type'] = 'page';
        $_p['comment_status'] = 'closed';
        $_p['ping_status'] = 'closed';
        $_p['post_category'] = array(1); // the default 'Uncategorized'

        // Insert the post into the database
        $the_page_id = wp_insert_post($_p);
    } else {
        // the plugin may have been previously active and the page may just be trashed...
        $the_page_id = $the_page->ID;

        // make sure the page is not trashed...
        $the_page->post_status = 'publish';
        $the_page_id = wp_update_post($the_page);
    }

    delete_option('gns_checkout_page_id');
    add_option('gns_checkout_page_id', $the_page_id);
}
register_activation_hook(__FILE__, 'gns_checkout_install');

/**
 * Runs on plugin deactivation
 */
function gns_checkout_remove() {
    global $wpdb;

    unregister_post_type('gns_checkout');

    $the_page_title = get_option("gns_checkout_page_title");
    $the_page_name = get_option("gns_checkout_page_name");

    // the id of our page...
    $the_page_id = get_option('gns_checkout_page_id');
    if ($the_page_id) {
        wp_delete_post($the_page_id, true); // this will trash, not delete
    }

    delete_option("gns_checkout_page_title");
    delete_option("gns_checkout_page_name");
    delete_option("gns_checkout_page_id");
}
register_deactivation_hook(__FILE__, 'gns_checkout_remove');

/**
 * Enqueue front end scripts and styles specific to this plugin
 */
function gns_checkout_scripts_styles() {
    wp_enqueue_style('gns-checkout-style', plugins_url('/css/', __FILE__) . 'gns-checkout-style.css');
    wp_enqueue_script('gns-checkout-js', plugins_url('/js/', __FILE__) . 'gns-checkout.js');
}
add_action('wp_enqueue_scripts', 'gns_checkout_scripts_styles', 99, 2);

/**
 * Enqueue admin scripts and styles specific to this plugin
 */
function gns_checkout_admin_style($hook) {
    /*if (
        $hook != 'toplevel_page_gns-checkout-settings-page' &&
        !$_GET['post_type'] == 'gns_checkout'
    ) {
        return;
    }*/
    wp_enqueue_style('custom_wp_admin_css', plugins_url('/css/gns-checkout-admin-style.css', __FILE__));
    //wp_enqueue_script('my_custom_script', get_template_directory_uri() . '/myscript.js');
}
add_action('admin_enqueue_scripts', 'gns_checkout_admin_style', 99, 2);
